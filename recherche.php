<?php include_once("classes/bd.php");
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Recherche - Collection films</title>
    <link rel="stylesheet" href="static/css/style.css" charset="utf-8">
  </head>
  <body>
    <h1>Recherche</h1>
    <form class="" action="films.php" method="get">

      <table>
        <tr>
          <td class="r_left">Titre:</td>
          <td><input type="text" name="Titre" placeholder="Titre..."></td>
        </tr>
        <tr>
          <td class="r_left">Réalisateur:</td>
          <td><input type="text" name="NomReal" placeholder="Réalisateur..."></td>
        </tr>
        <tr>
          <td class="r_left">Annee:</td>
          <td><input type="number" name="Annee" min="1800" max ="2016" placeholder="2016"></td>
        </tr>
        <tr>
          <td class="r_left">Genre:</td>
          <td><select name="NomGenre">
            <?php
              $BD = new BD();
              $genres = $BD->getGenres();
              echo '<option value=""></option>';
              foreach($genres as $g){
                echo '<option value="'.$g.'">'.ucfirst($g).'</option>';
              }
            ?>
          </select></td>
        </tr>
      </table>
      <button type="button" id='valider'>Rechercher</button>
      <?php
      echo '<a href="index.php">Accueil</a>'; ?>
      <script src="static/js/jquery-3.1.1.js" charset="utf-8"></script>
      <script src="static/js/recherche.js" charset="utf-8"></script>
    </form>
  </body>
</html>
