<!doctype html>
<html lang='fr'>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/accueil.css" />
  <title>Collection de films</title>
</head>
<body>
  <header>
  </header>

  <h1>Collection</h1>

  <section>
  <p>
  <form method="get" action="recherche.php">
  <input type="submit" value="Recherche"></input>
  </form>
  </p>

  <p>
  <form method="get" action="insertFilm.php">
  <input type="submit" value="Ajouter Film"></input>
  </form>
  </p>

  <p>
  <form method="get" action="SupprFilm.php">
  <input type="submit" value="Supprimer Film"></input>
  </form>
  </p>
  </section>

  <h1>Liste des films : </h1>
  <?php
  include_once("classes/bd.php");
    $BD = new BD();
    $titre = $_GET["Titre"];
    $real = $_GET["nomReal"];
    $genre = $_GET["nomGenre"];

    echo "<ul>";
    $real = $_GET['real'];
    // $r = $BD->getFilmsReal($real);
    $r = $BD->bd->query(makeReq());
    foreach($r as $rq){
      echo "<li>".$rq["Titre"]." - ". $rq["Annee"] ;
      echo "<br/> description: ".$rq["Description"]."</li><br/><br/>";
    }
    echo "</ul>";
function makeReq(){
  $req = "SELECT * FROM Film";
  if(isset($_GET['NomReal']))
    $req = $req." natural join Realisateur";
  if(isset($_GET['NomGenre']))
    $req = $req." natural join Genre";
  $nbQ = 0;
  foreach ($_GET as $var => $value) {
    $nbQ += 1;
  }
  if($nbQ != 0)
    $req = $req." where ";

  foreach ($_GET as $var => $value) {
    $req = $req." ".$var." LIKE '%".$_GET[$var]."%'";
    if($var != "NomGenre" )
      $req = $req." and";
  }
  if(!isset($_GET["NomGenre"])){
    $req = rtrim($req, " and");
  }

  return $req;

}
?>

  <footer>
  </footer>
</body>
<html>
