<?php

  function getGenres(){
    try {
      $db = new PDO('sqlite:bd/base.sqlite');
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
   catch (PDOExcepio $e) {
      die("pas de co");
  }
    $req = $db->query('select * from GENRE');
    $res = [];
    foreach($req as $q){
      $res[]= strval($q['NOM_GENRE']);
    }
    return $res;
  }

  function getReals($nomR){
    try {
      $db = new PDO('sqlite:bd/base.sqlite');
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
     catch (PDOExcepio $e) {
        die("pas de co");
    }
    $req = $db->query('select NOM_REAL from REALISATEUR where NOM_REAL=\''.$nomR.'\'');
    $res = array();
    foreach($req as $q){
      array_push($res, strval($q['NOM_REAL']));
    }
    return $res;
  }

  function getIdReal($nomR){
    try {
      $db = new PDO('sqlite:bd/base.sqlite');
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
     catch (PDOExcepio $e) {
        die("pas de co");
    }
    $query = isset($real) ? "SELECT ID_REALISATEUR FROM REALISATEUR WHERE NOM_REAL='".$nomR."'" :
                            "SELECT * FROM REALISATEUR";
    $req = $db->query($query);
    return strval($req[0]);
  }

  function getFilmsReal($nomR){
    try {
      $db = new PDO('sqlite:bd/base.sqlite');
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
     catch (PDOExcepio $e) {
        die("pas de co");
    }

    if(isset($nomR))
      $query =  "SELECT TITRE, ANNEE FROM FILM natural join REALISATEUR WHERE NOM_REAL='".$nomR."'";
    else
      $query = "SELECT * FROM REALISATEUR";
    $req = $db->query($query);
    $res = [];
    foreach($req as $r){
      $res[] =array('TITRE'=> strval($r["TITRE"]),
                    'ANNEE'=> strval($r["ANNEE"]));
    }
    return $res;
  }
}
 ?>
