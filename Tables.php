<?php
date_default_timezone_set('Europe/Paris');

try{
  // Creation de la SQLite
  $file_db = new PDO('sqlite:biblioFilm.sqlite3');
  //Gerer le niveau des erreurs rapportées
  $file_db->setAttribute(PDO::ATTR_ERRMODE,
                          PDO::ERRMODE_EXCEPTION);

 $file_db->exec("DROP TABLE Film ");
 echo "<p>drop table Film</p>";
 $file_db->exec("DROP TABLE Genre ");
 echo "<p>drop table genre</p>";
 $file_db->exec("DROP TABLE Realisateur");
 echo "<p>drop table Realisateur</p>";



$file_db->exec("CREATE TABLE IF NOT EXISTS Realisateur (
  IdReal INTEGER PRIMARY KEY,
  NomReal TEXT,
  Prenom TEXT)");

echo "<p>table Realisateur crée</p>";

$file_db->exec("CREATE TABLE IF NOT EXISTS Genre(
  IdGenre INTEGER PRIMARY KEY,
  NomGenre TEXT)");
  echo "<p>table Realisateur Genre</p>";


  $file_db->exec("CREATE TABLE IF NOT EXISTS Film (
    IdFilm INTEGER PRIMARY KEY,
    Titre TEXT,
    Annee TEXT,
    Description TEXT,
    IdReal INTEGER,
    IdGenre INTEGER,
    FOREIGN KEY (IdReal) REFERENCES Realisateur(IdReal),
    FOREIGN KEY (IdGenre) REFERENCES Genre (IdGenre))");
    echo "<p>table Film crée</p>";


  $file_db->exec("INSERT INTO Realisateur (IdReal,nomReal,prenom) VALUES (0, 'Spielberg', 'Steven')");
  $file_db->exec("INSERT INTO Realisateur (IdReal,nomReal,prenom) VALUES (1, 'Reiner', 'Rob')");

  $file_db->exec("INSERT INTO Genre (IdGenre, NomGenre) VALUES (1,'Comédie')");
  $file_db->exec("INSERT INTO Genre (IdGenre, NomGenre) VALUES (2, 'Horreur')");
  $file_db->exec("INSERT INTO Genre (IdGenre, NomGenre) VALUES (3,'Fantasy')");
  $file_db->exec("INSERT INTO Genre (IdGenre, NomGenre) VALUES (4,'Action')");
  $file_db->exec("INSERT INTO Genre (IdGenre, NomGenre) VALUES (5,'Policier')");
  $file_db->exec("INSERT INTO Genre (IdGenre, NomGenre) VALUES (6,'Animé')");

  $file_db->exec("INSERT INTO Film (IdFilm,Titre,Annee, Description, IdReal, IdGenre) VALUES (0, 'Princess Bride', 1987, 'Princess Bride ou La Princesse Bouton d or au Québec est une comédie fantastique et romantique américaine de Rob Reiner, sortie en 1987 et adapté du roman éponyme de William Goldman paru en 1973', 1, 3)");
  $file_db->exec("INSERT INTO Film (IdFilm,Titre,Annee, Description, IdReal, IdGenre) VALUES (1, 'Jurassic Park', 1993, 'Jurassic Park, ou Le Parc jurassique au Québec et au Nouveau-Brunswick, est un film américain d aventure et de science-fiction réalisé par Steven Spielberg, sorti le 11 juin 1993 et adapté du roman éponyme de Michael Crichton paru en 1990.', 0, 3)");


  $file_db = null;




  }
  catch(PDOException $e) {
    echo $e->getMessage();
  }
?>
