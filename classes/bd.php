<?php
  class BD{
    var $bd;
    public function __construct(){
      try {
        $this->bd = new PDO('sqlite:biblioFilm.sqlite3');
        $this->bd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      }
       catch (PDOExcepio $e) {
          die("pas de co");
      }
    }

    public function getGenres(){
      $req = $this->bd->query('select * from Genre');
      $res = [];
      foreach($req as $q){
        $res[]= strval($q['NomGenre']);
      }
      return $res;
    }

    public function getReals($nomR){
      $req = $this->bd->query('select NomReal from Realisateur where NomReal=\''.$nomR.'\'');
      $res = array();
      foreach($req as $q){
        array_push($res, strval($q['nomReal']));
      }
      return $res;
    }

    public function getIdReal($nomR){
      $query = isset($real) ? "SELECT IdReal FROM Realisateur WHERE NomReal='".$nomR."'" :
                              "SELECT * FROM Realisateur";
      $req = $this->bd->query($query);
      return strval($req[0]);
    }

    public function getFilmsReal($nomR){

      if(isset($nomR))
        $query =  "SELECT Titre, Annee FROM Film natural join Realisateur WHERE NomReal='".$nomR."'";
      else
        $query = "SELECT Titre, Annee FROM Film";
      $req = $this->bd->query($query);
      $res = [];
      foreach($req as $r){
        $res[] =array('TITRE'=> strval($r["Titre"]),
                      'ANNEE'=> strval($r["Annee"]));
      }
      return $res;
    }

  }
?>
