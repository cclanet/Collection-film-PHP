<?php include_once("classes/bd.php");?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Films - Collection films</title>
  </head>
  <body>
    <h1>Résultats de la recherche.</h1>
    <?php
      $BD = new BD();
      $titre = $_GET["Titre"];
      $real = $_GET["nomReal"];
      $genre = $_GET["nomGenre"];

      echo "<ul>";
      $real = $_GET['real'];
      // $r = $BD->getFilmsReal($real);
      $r = $BD->bd->query(makeReq());
      foreach($r as $rq){
        echo "<li>".$rq["Titre"]." - ". $rq["Annee"] ;
        echo "<br/> description: ".$rq["Description"]."</li><br/><br/>";
      }
      echo "</ul>";

      echo '<a href="index.php">Accueil</a>';
     ?>
  </body>
</html>
<?php
  function makeReq(){
    $req = "SELECT * FROM Film";
    if(isset($_GET['NomReal']))
      $req = $req." natural join Realisateur";
    if(isset($_GET['NomGenre']))
      $req = $req." natural join Genre";
    $nbQ = 0;
    foreach ($_GET as $var => $value) {
      $nbQ += 1;
    }
    if($nbQ != 0)
      $req = $req." where ";

    foreach ($_GET as $var => $value) {
      $req = $req." ".$var." LIKE '%".$_GET[$var]."%'";
      if($var != "NomGenre" )
        $req = $req." and";
    }
    if(!isset($_GET["NomGenre"])){
      $req = rtrim($req, " and");
    }

    return $req;

  }
?>
